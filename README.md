#### ASACO CONFIG ####

-- VAGRANT DEVELOPER'S MANUAL --

after git pull...

1. move to asacoconfig directory
2. vagrant up

commands:

- connect to machine: vagrant ssh
- stop machine: vagrant halt
- suspend machine: vagrant suspend
- resume machine: vagrant resume
- erase machine: vagrant destroy

