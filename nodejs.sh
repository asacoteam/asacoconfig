#!/usr/bin/env bash

curl --silent --location https://rpm.nodesource.com/setup_9.x | sudo bash -
sudo yum -y install nodejs
sudo yum install -y gcc-c++ make
