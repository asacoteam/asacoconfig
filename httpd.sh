#!/usr/bin/env bash

sudo yum install -y httpd
if ! [ -L /var/www ]; then
  rm -rf /var/www/html
  ln -fs /vagrant/html /var/www/html
fi
sudo systemctl start httpd
